from django.test import TestCase
from django.test import Client
from django.urls import resolve
from .views import profile


# Create your tests here.
class ProfilePageUnitTest(TestCase):
    def test_url_profile_is_exist(self):
        response = Client().get('/profile/')
        self.assertEqual(response.status_code, 200)

    def test_url_profile_using_profile_function(self):
        found = resolve('/profile/')
        self.assertEqual(found.func, profile)

    def test_page_contains_name(self):
        response = Client().get('/profile/')
        html_response = response.content.decode('utf8')
        self.assertIn('George', html_response)

    def test_page_contains_npm(self):
        response = Client().get('/profile/')
        html_response = response.content.decode('utf8')
        self.assertIn('1706043891', html_response)

    def test_profile_page_using_templates(self):
        response = Client().get('/profile/')
        self.assertTemplateUsed(response, 'profile.html')

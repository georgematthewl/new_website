$(document).ready(function () {
    $(window).on('load', function () {
        $('#status').delay(800).fadeOut();
        $('#preloader').delay(1000).fadeOut('slow');
    });

    $("#accordion").accordion({
        collapsible: true,
        active: false,
        heightStyle: "content",
    });

    $('textarea').autoResize();

    var area = document.getElementById("content-text");
    var counter = document.getElementById("counter");
    var maxLength = 300;
    var checkLength = function () {
        if (area.value.length <= maxLength) {
            counter.innerHTML = (maxLength - area.value.length) + " characters remaining";
        }
    };
    setInterval(checkLength, 50);

    var local = window.localStorage;

    if (local.getItem('isDark') === 'true') {
        $('.result > div').removeClass('result-box');
        $('.result > div').addClass('result-box-2');
        $('#body').removeClass('body');
        $('#body').addClass('body-2');
        $('#form-status').removeClass('form-status');
        $('#form-status').addClass('form-status-2');
        $('#counter').css('color', 'white');
        document.getElementById('title').style.color = 'white';
        document.getElementById('status-title').style.color = 'white';
    } else {
        $('.result > div').addClass('result-box');
        $('.result > div').removeClass('result-box-2');
        $('#body').addClass('body');
        $('#body').removeClass('body-2');
        $('#form-status').addClass('form-status');
        $('#form-status').removeClass('form-status-2');
        $('#counter').css('color', '#555555');
        document.getElementById('title').style.color = 'black';
        document.getElementById('status-title').style.color = 'black';
    }

    document.getElementById('change-theme').onclick = changeTheme;

    function changeTheme() {
        if ($("body").hasClass('body')) {
            $('.result > div').removeClass('result-box');
            $('.result > div').addClass('result-box-2');
            $('#body').removeClass('body');
            $('#body').addClass('body-2');
            $('#form-status').removeClass('form-status');
            $('#form-status').addClass('form-status-2');
            $('#counter').css('color', 'white');
            document.getElementById('title').style.color = 'white';
            document.getElementById('status-title').style.color = 'white';
            local.setItem('isDark', true);

        } else {
            $('.result > div').addClass('result-box');
            $('.result > div').removeClass('result-box-2');
            $('#body').addClass('body');
            $('#body').removeClass('body-2');
            $('#form-status').addClass('form-status');
            $('#form-status').removeClass('form-status-2');
            $('#counter').css('color', '#555555');
            document.getElementById('title').style.color = 'black';
            document.getElementById('status-title').style.color = 'black';
            local.setItem('isDark', false);
        }
    }
});

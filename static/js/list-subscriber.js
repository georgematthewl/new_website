var email = "";

function changeEmail(self) {
    email = self.id;
}

function unsubscribe() {
    var csrftoken = $("[name=csrfmiddlewaretoken]").val();
    var password = $('#input-password').val();

    $.ajax({
        method: 'POST',
        url: '/subscribe/delete/',
        headers: {
            "X-CSRFToken": csrftoken,
        },
        data: {email: email, password: password},
        success: function (response) {
            if (response.deleted) {
                $('#close').click();
                $("[name=" + "'" + email + "']").remove();
            } else {
                $('.modal-body .errorlist').replaceWith("<p class='errorlist fail'>Wrong password!</p>");
            }
        }
    })
}

$(function () {
    var csrftoken = $("[name=csrfmiddlewaretoken]").val();

    $('body').onload = renderResult();

    function renderResult() {
        $.ajax({
            method: 'POST',
            url: '/subscribe/render/',
            headers: {
                "X-CSRFToken": csrftoken,
            },
            success: function (result) {
                var data_length = result.data.length;
                var data = result.data;

                for (var i = 0; i < data_length; i++) {
                    var name = data[i]['name'];
                    var email = data[i]['email'];
                    var button = `<button type="button" class="btn btn-primary" data-toggle="modal" data-target="#myModal" onclick="changeEmail(this)" id="${email}">Unsubscribe</button>`;

                    var html =
                        `<tr name="${email}"><td>${name}</td><td>${email}</td><td align='center'>${button}</td></tr>`;
                    $('#tbody').append(html);
                }
            }
        })
    }
});
from django import forms


class StatusForm(forms.Form):
    error_messages = {
        'required': 'Tolong isi input ini!',
    }

    status_message = forms.CharField(label='Status:', max_length=300,
                                     widget=forms.Textarea(
                                         attrs={'name': 'status-message', 'placeholder': "What's your status?",
                                                'class': 'text-field',
                                                'id': 'content-text'}))

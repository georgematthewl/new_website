from django.db import models


# Create your models here.
class StatusInput(models.Model):
    status_message = models.CharField(max_length=300)
    created_at = models.DateTimeField(auto_now_add=True)

    def __str__(self):
        return self.status_message

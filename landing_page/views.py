from django.shortcuts import render
from django.http import HttpResponseRedirect
from .forms import StatusForm
from .models import StatusInput

response = {}


# Create your views here.
def index(request):
    list_status = list(StatusInput.objects.all())
    list_status.reverse()
    response['result'] = list_status
    response['form'] = StatusForm
    return render(request, 'landing_page/landing-page.html', response)


def add_result(request):
    form = StatusForm(request.POST)
    if request.method == 'POST' and form.is_valid():
        data = form.cleaned_data
        StatusInput.objects.create(**data)
        return HttpResponseRedirect('/')
    else:
        return HttpResponseRedirect('/')

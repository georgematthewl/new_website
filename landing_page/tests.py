import time
from django.test import TestCase, LiveServerTestCase
from django.test import Client
from django.urls import resolve
from .views import index as landing_page
from .models import StatusInput
from .forms import StatusForm
from selenium import webdriver
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.chrome.options import Options


# Create your tests here.
class LandingPageUnitTest(TestCase):
    def test_url_landing_page_is_exist(self):
        response = Client().get('/')
        self.assertEqual(response.status_code, 200)

    def test_landing_page_using_index_function(self):
        found = resolve('/')
        self.assertEqual(found.func, landing_page)

    def test_model_can_add_new_status(self):
        status_message = StatusInput.objects.create(status_message='Halo, saya lagi belajar PPW')

        counting_all_status_message = StatusInput.objects.all().count()
        self.assertEqual(counting_all_status_message, 1)

    def test_if_form_is_blank(self):
        form = StatusForm(data={'status_message': ''})
        self.assertFalse(form.is_valid())
        self.assertEqual(
            form.errors['status_message'][0],
            'This field is required.',
        )

    def test_form_post_and_render_the_result(self):
        test = 'Anonymous'
        response_post = Client().post('/result/', {'status_message': test})
        self.assertEqual(response_post.status_code, 302)

        response = Client().get('/')
        html_response = response.content.decode('utf8')
        self.assertIn(test, html_response)

    def test_form_post_error_and_render_the_result(self):
        test = 'Anonymous'
        response_post = Client().post('/result/', {'status_message': ''})
        self.assertEqual(response_post.status_code, 302)

        response = Client().get('/')
        html_response = response.content.decode('utf8')
        self.assertNotIn(test, html_response)

    def test_self_func(self):
        StatusInput.objects.create(status_message='HELLO')
        status = StatusInput.objects.get(id=1)
        self.assertEqual(str(status), status.status_message)

    def test_text_max_length(self):
        status = StatusInput.objects.create(status_message="TEST TEST TEST TEST TEST TEST")
        self.assertLessEqual(len(str(status)), 300)


class LandingPageFunctionalTest(LiveServerTestCase):
    def setUp(self):
        chrome_options = Options()
        chrome_options.add_argument('--dns-prefetch-disable')
        chrome_options.add_argument('--no-sandbox')
        chrome_options.add_argument('--headless')
        chrome_options.add_argument('disable-gpu')
        self.selenium = webdriver.Chrome('./chromedriver', chrome_options=chrome_options)
        super(LandingPageFunctionalTest, self).setUp()

    def test_input_and_result_in_same_page(self):
        selenium = self.selenium
        # Opening the link we want to test
        selenium.get(self.live_server_url)
        # Find the form element
        status_message = selenium.find_element_by_name('status_message')
        submit = selenium.find_element_by_id('submit')
        time.sleep(3)

        # Fill the form with data
        status_message.send_keys('Coba Coba')

        # Submitting the form
        submit.send_keys(Keys.RETURN)

        self.assertIn("Status!", selenium.title)
        self.assertIn("Coba Coba", selenium.page_source)

    def test_header_is_correct(self):
        selenium = self.selenium
        selenium.get('http://your-status-page.herokuapp.com')
        time.sleep(3)

        head_tag = selenium.find_element_by_id('title').text
        self.assertIn('Hello! Apa Kabar?', head_tag)

    def test_profile_button_is_correct(self):
        selenium = self.selenium
        selenium.get('http://your-status-page.herokuapp.com')
        time.sleep(3)

        button = selenium.find_element_by_id('profile').text
        self.assertIn("MY PROFILE", button)

    def test_form_status_class_use_display_flex(self):
        selenium = self.selenium
        selenium.get('http://your-status-page.herokuapp.com')
        time.sleep(3)

        form = selenium.find_element_by_id('form-status').value_of_css_property('display')
        self.assertIn(form, 'flex')

    def test_result_box_bg_color_is_white(self):
        selenium = self.selenium
        selenium.get('http://your-status-page.herokuapp.com')
        time.sleep(3)

        result = selenium.find_element_by_class_name('result-box').value_of_css_property('background-color')
        self.assertIn(result, "rgba(255, 255, 255, 1)")

    def test_change_theme_button(self):
        selenium = self.selenium
        selenium.get("http://your-status-page.herokuapp.com")
        time.sleep(3)

        button = selenium.find_element_by_id('change-theme')
        button.send_keys(Keys.RETURN)

        main_container = selenium.find_element_by_id('body').get_attribute('class')
        self.assertIn('body-2', main_container)

    def test_accordion(self):
        selenium = self.selenium
        selenium.get('http://your-status-page.herokuapp.com/profile')
        time.sleep(3)

        button_acc = selenium.find_element_by_id('ui-id-1')
        section = selenium.find_element_by_id('ui-id-1').get_attribute('aria-selected')
        nextSec = selenium.find_element_by_id('ui-id-2').get_attribute('aria-selected')
        button_acc.send_keys(Keys.RETURN)
        self.assertNotEqual(section, nextSec)

    def tearDown(self):
        time.sleep(3)
        self.selenium.quit()
        super(LandingPageFunctionalTest, self).tearDown()

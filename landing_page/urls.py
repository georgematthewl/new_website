from django.urls import path
from .views import index as landing_page
from .views import add_result

urlpatterns = [
    path('', landing_page, name="landing-page"),
    path('result/', add_result, name="add-result"),
]

from django.http import JsonResponse
from django.shortcuts import render
from .forms import SubscribeForm
from .models import SubscriberData


# Create your views here.


def subscribe(request):
    return render(request, 'subscribe.html', {'form': SubscribeForm})


def list_subscriber(request):
    return render(request, 'list-subscriber.html')


def validate_email(request):
    if request.method == 'POST':
        email = request.POST['email']
        check_email = SubscriberData.objects.filter(email=email)
        if check_email.exists():
            return JsonResponse({'is_exists': True})
        return JsonResponse({'is_exists': False})


def save_to_model(request):
    if request.method == 'POST':
        name = request.POST['name']
        email = request.POST['email']
        password = request.POST['password']
        SubscriberData.objects.create(name=name, email=email, password=password)
        return JsonResponse({'is_success': True, 'data': list(SubscriberData.objects.all().values())})


def render_subscriber_list(request):
    if request.method == 'POST':
        data = list(SubscriberData.objects.all().values())
        return JsonResponse({'data': data})


def delete_subscriber(request):
    if request.method == 'POST':
        email = request.POST['email']
        password = request.POST['password']
        data = SubscriberData.objects.filter(email=email, password=password)
        if data.exists():
            data.delete()
            return JsonResponse({'deleted': True})
        else:
            return JsonResponse({'deleted': False})

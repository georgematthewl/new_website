from django.apps import AppConfig


class SubscribePageConfig(AppConfig):
    name = 'subscribe_page'

from django.urls import path
from .views import subscribe, validate_email, save_to_model, list_subscriber, render_subscriber_list, delete_subscriber

urlpatterns = [
    path('', subscribe, name="subscribe"),
    path('validate/', validate_email, name="validate"),
    path('post/', save_to_model, name="post"),
    path('subscriber/', list_subscriber, name="subscriber-page"),
    path('render/', render_subscriber_list, name="render"),
    path('delete/', delete_subscriber, name="delete"),
]

from django import forms
from .models import SubscriberData


class SubscribeForm(forms.ModelForm):
    class Meta:
        model = SubscriberData
        fields = '__all__'

    name = forms.CharField(label="Full Name", max_length=100,
                           widget=forms.TextInput(
                               attrs={'id': 'name', 'class': 'form-control', 'placeholder': 'Enter your full name'}))
    email = forms.EmailField(label="Email", max_length=100,
                             widget=forms.EmailInput(attrs={'id': 'email', 'class': 'form-control',
                                                            'placeholder': 'Enter your email'}))
    password = forms.CharField(label='Password', max_length=30, widget=forms.PasswordInput(
        attrs={'id': 'password', 'class': 'form-control', 'placeholder': 'Enter your password'}))

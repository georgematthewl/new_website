from django.db import IntegrityError
from django.test import TestCase, Client
from django.urls import resolve
from .views import *
from .models import SubscriberData


# Create your tests here.
class SubscribePageUnitTest(TestCase):
    def test_url_subscribe_page_is_exist(self):
        response = Client().get('/subscribe/')
        self.assertEqual(response.status_code, 200)

    def test_url_subscriber_list_page_is_exists(self):
        response = Client().get('/subscribe/subscriber/')
        self.assertEqual(response.status_code, 200)

    def test_url_subscribe_using_subscribe_function(self):
        found = resolve('/subscribe/')
        self.assertEqual(found.func, subscribe)

    def test_model_can_save_data(self):
        SubscriberData.objects.create(name="Tester", email='test@ui.ac.id', password="asdasd123")

        counting_all_data = SubscriberData.objects.all().count()
        self.assertEqual(counting_all_data, 1)

    def test_self_func_name(self):
        SubscriberData.objects.create(name='Test', email='a@a.com', password='123')
        subscriber = SubscriberData.objects.get(email='a@a.com')
        self.assertEqual(str(subscriber), subscriber.email)

    def test_unique_email(self):
        SubscriberData.objects.create(email="test@email.com")
        with self.assertRaises(IntegrityError):
            SubscriberData.objects.create(email="test@email.com")

    def test_post_using_ajax(self):
        response = Client().post('/subscribe/post/', data={
            "name": "jorj",
            "email": "jorj@gmail.com",
            "password": "123",
        })
        self.assertEqual(response.status_code, 200)

    def test_check_email_view_get_return_200(self):
        response = Client().post('/subscribe/validate/', data={
            "email": "emailku@gmail.com"
        })
        self.assertEqual(response.status_code, 200)

    def test_check_email_already_exist_view_get_return_200(self):
        SubscriberData.objects.create(name="jorj",
                                      email="jorj@gmail.com",
                                      password="123")
        response = Client().post('/subscribe/validate/', data={
            "email": "jorj@gmail.com"
        })
        self.assertEqual(response.json()['is_exists'], True)

    def test_delete_email(self):
        SubscriberData.objects.create(name='george', email='hehe@gmail.com', password='pass123')
        post = Client().post('/subscribe/delete/',
                             data={'name': 'george', 'email': 'hehe@gmail.com', 'password': 'pass123'})
        self.assertEqual(post.json()['deleted'], True)

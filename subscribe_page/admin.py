from django.contrib import admin
from .models import SubscriberData

# Register your models here.
admin.site.register(SubscriberData)

from django.db import models


# Create your models here.
class SubscriberData(models.Model):
    name = models.CharField(max_length=200)
    email = models.EmailField(max_length=200, primary_key=True, unique=True)
    password = models.CharField(max_length=50)

    def __str__(self):
        return self.email
